<?php

namespace CTSI\EPIC;
use CTSI\EPIC\HL7Client;
use REDCap;

require_once __DIR__ . '/../init.php';
require_once __DIR__ . '/../inc/constants.php';
/**
 *
 * This class is used in either an individual record setting and called by a save hook, or is is run as a batch every two minutes 
 * to send any queued messages or find new ones that need to be queued.  
 * 
 * NOTE: Messages are only sent from the queue project.  This was implemented because performance of the message send could delay the performance of redcap_save_record
 * which impacted uses.  Now a fast save is made to the queue project and sends are managed asynchronously by a batch method calling sendLog.
 * 
 * 
 * The public methods are:
 * FlintRegistryPreSave
 * FlintRegistrySaveHook
 * FlintRecordUpdate
 * sendLog
 *  
 */
class EpicSync
{

	private $origData;
	private $adtClient;
	private $flowClient;
	private $adtQueue;
	private $flowQueue;
	private $guids;
	private $registryUpdates;
	private $typeLookup;

	public function __construct() {

		$this->adtQueue = array();
		$this->flowQueue = array();
		$this->registryUpdates = array();
		$this->adtClient = new HL7Client(EPIC_HL7_HOST, EPIC_ADT_PORT, 'ADT');
		$this->flowClient = new HL7Client(EPIC_HL7_HOST, EPIC_FLOW_PORT, 'FLOW');
		$this->typeLookup = array('adt'=> 0, 'flow' =>1);
		$this->guids = array();
		$this->origData = new \stdClass();
	}
	public function __destruct() {
		//The sync object functions as a queue that stores all the messages, and then sends them at destruction time
		$logs = array();  //the logs of the result of the sent HL7.  These will be written to the log project of all the messages
		
		log_ts('processing ADT queue of ' .sizeof($this->adtQueue) );

		//Since Epic needs to have the ADT before the flow sheet can be received.  This forces all the ADT to be sent first
		if(\sizeof($this->adtQueue)) {
			foreach($this->adtQueue as $record){
				$logs[] = $this->sendRecordToLog($record, 'adt');
				$this->pushUpdateToRegistry($record);  //mark the record that Epic has been updated
			}
		}

		log_ts('processing flow queue of ' .sizeof($this->flowQueue) );
		if(\sizeof($this->flowQueue)){
			foreach($this->flowQueue as $record){
				$logs[] = $this->sendRecordToLog($record, 'flow');
				$this->pushUpdateToRegistry($record); //mark the record that Epic has been updated
			}
		}
		
		log_ts('saving ' . sizeof( $logs). ' HL7 log updates');
		//write the updates to the log of messages.  This can be success or failure
		$res = REDCap::saveData(EPIC_MESSAGE_LOG_PROJECT_ID, 'json', json_encode($logs));
		if(array_key_exists( 'errors', $res) && \sizeof($res['errors']) == 0 ) {  //no errors in the updates to the log
			if( sizeof( $this->registryUpdates ) > 0){
				
				$rs = $this->var_dump_ret($res);
				log_ts('saving ' . sizeof( $this->registryUpdates ). ' registry updates\n'.$rs);
				REDCap::logEvent("Epic Sync",  "Saving ". sizeof( $this->registryUpdates ). ' registry updates');
				//update the registry with the sent times and reset force updates for all records
				$res = REDCap::saveData(TARGET_PROJECT_ID, 'json', \json_encode($this->registryUpdates), 'overwrite', null, null, null, TRUE, FALSE);
			}
			log_ts('done');	
		} 
		else {
			log_ts('HL7 Send failed, subjects not updated');
			var_dump($res);
		}
	}


	public function FlintRegistryPreSave($project_id, $record) {
		//pull all the relevent data and store to be compaired to the value after the rest of the triggers fire (collapse variables)
		$j = json_decode(REDCap::getData($project_id, 'json', $record, REDCap::getFieldNames(array('individual_demographics', 'data_to_transcribe_to_epic', 'communications', 'year_1_tracking', 'invitation_tracker', 'pulse_and_wave_tracking')), BASELINE_EVENT));
		$this->origData = $j[0];

	}
	public function FlintRegistrySaveHook($project_id, $record_id, $instrument = null) {

		$recs = json_decode(REDCap::getData($project_id, 'json', $record_id, REDCap::getFieldNames(array('individual_demographics', 'data_to_transcribe_to_epic', 'communications','year_1_tracking', 'invitation_tracker', 'pulse_and_wave_tracking')), BASELINE_EVENT));
		$record = $recs[0];
		if($instrument == 'communications'){
			//the communication form was saved.  Mark the record as forced hl7 update
			$record->force_hl7_update = 1;
		}
		$this->queueRecord($record);
	}

	public function FlintRecordUpdate($project_id, $record){ //this is expecting a record object
		//this call immediately queues the record to be sent.  It does not check if that has been a change in the record.  The reason for this is that is
		//called as a batch job.  It sends and updates over the interface that were imported rather than entered.
		$this->origData = new \stdClass();
		$this->queueRecord($record);
	}


	public function sendLog(){
		$logs = array();
		log_ts('getting log of messages to send');
		//get all messages that are not marked as success
		$j = json_decode(REDCap::getData(EPIC_MESSAGE_LOG_PROJECT_ID, 'json',null, array('record_id', 'frid','ts', 'obj', 'type'), null, null, false, false, false, "[obj] != '' and [success] != 1"));
		usort($j, array($this, 'orderMessages'));
		//sorted by ADT first, then by FRID, finally TS
		$prev_type = -1;
		foreach($j as $logEntry){
			if($logEntry->type ==1 && $prev_type == 0){ // we are on a flow message and have had an adt message
				sleep(5); //wait for EPIC to create all the records for the ADT messages that were just sent
			}
			if($logEntry->type == 0){
				 $logs[] = $this->attemptSend($logEntry, $this->adtClient);
			}
			else {
				$logs[] = $this->attemptSend($logEntry, $this->flowClient);
			}
			$prev_type = $logEntry->type; 
		}
		log_ts('saving results of hl7 send');

		//save the results of the send back to the log project
		$res = REDCap::saveData(EPIC_MESSAGE_LOG_PROJECT_ID, 'json', json_encode($logs));
		log_ts('done sending hl7');
	}

	
	private function var_dump_ret($mixed = null) {
		ob_start();
		var_dump($mixed);
		$content = ob_get_contents();
		ob_end_clean();
		return $content;
	}

	private function pushUpdateToRegistry($record){  //push the update onto the queue of updates to make to the registry project
		$frid = $record->frid;
		if(!isset($this->registryUpdates[$frid])){
			$update = new \stdClass();
			$update->frid = $frid;
			$update->dt_transcribed_to_epic = date('Y-m-d H:i:s');
			$update->redcap_event_name = BASELINE_EVENT;
			if($record->force_hl7_update) {
				//force was set to true, mark it as false, since the update has now been queued
				$update->force_hl7_update = 0;
			}

			$this->registryUpdates[$update->frid] = $update;
		}
	}


	private function queueRecord($record){
		if($record->dup != 1 && $record->ts != ''){  //this record is not a duplicate and ts is set (from pre-enroll), then messages can be queued
			if($record->dt_transcribed_to_epic == ''){  //the record has never been sent to epic, so but the adt and flow messages must be queued
				 $this->queueMessage($record, $this->adtQueue);
				 $this->queueMessage($record, $this->flowQueue);
			}
			else if( $this->flowChanged($record) ){
				 $this->queueMessage($record, $this->flowQueue);
			}
		}
	}

	private function flowChanged($currentData){
		foreach($this->flowClient->getFields() as $field){  //check all the fields that could be sent through the flow sheet
			if($currentData->{$field} != $this->origData->{$field}){  //if the current value is not the same as original, there was some change
				return true;
			}
		}
		if($currentData->force_hl7_update) {
			//force was set to true, flowChanged will be true to ensure it is sent
			return true;
		}
		return false;
	}

	private function queueMessage($record, &$queue){
		$queue[] = $record;
	}

	private function sendRecordToLog($record, $type) {
		$record->message_id = $this->getNextGUID(EPIC_MESSAGE_LOG_PROJECT_ID);
		$log = new \stdClass();
		$log->record_id = $record->message_id;
		$log->frid = $record->frid;
		$log->ts = date('Y-m-d H:i:s');
		$record->save_ts = date('YmdHis');
		//the record is seriliazed and saved into the obj field in the message queue project
		$log->obj = serialize($record);
		$log->type = $this->typeLookup[$type];
		return $log;
	}
	
	private function attemptSend($logEntry, $client) {
		//the record is seriliazed and saved into the obj field in the message queue project
		$res = $client->send(\unserialize($logEntry->obj));
		$logUpdate = new \stdClass();
		$logUpdate->record_id = $logEntry->record_id;
		$logUpdate->last_attempt = date('Y-m-d H:i:s');
		$logUpdate->sent = $res['sent'];
		$logUpdate->resp = $res['response'];
		$logUpdate->success = $res['success'];
		$logUpdate->error = $res['error'];
		
		return $logUpdate;
	}
	
	private function orderMessages($a, $b){
		//this is the sorting function for messages in the log that have not succeeded.  
		$type_compare = strcmp($a->type, $b->type);
		
		$frid_compare = strcmp($a->frid, $b->frid);
		if($type_compare == 0){
			if($frid_compare == 0){
				return strcmp($a->ts, $b->ts);
			}
			return $frid_compare;
		}
		return $type_compare;
	}

	private function getNextGUID($project_id) {
		$date_prefix = date('Ymd');  //date prefixes were added to the GUID (record id in message queue project) to make it faster to filter
		
		if(sizeof($this->guids) == 0){
				log_ts('getting all GUIDs');
				
				$filter = "starts_with([record_id], \"$date_prefix\")";
			  $res = json_decode(REDCap::getData($project_id, 'json',null, null, null, null, false, false, false, $filter));
				foreach ($res as $key => $v) {
					$this->guids[$v->record_id] = 1;
				
				}
				log_ts('done getting guids' . sizeof($this->guids));
		}

		while($guid = $date_prefix.self::GUID()) {  //make sure the GUID does not exist (rather than just statistically unlikely)
		if(!isset($this->guids[$guid])){
			$this->guids[$guid] = 1;
			return $guid;
		}

		}
	}

	private static function GUID()
	{
	    if (function_exists('com_create_guid') === true)
	    {
	        return trim(com_create_guid(), '{}');
	    }

	    return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
	}

}
