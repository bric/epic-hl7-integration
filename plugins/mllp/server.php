<?php
/**
 * Stub code to help with testing the sending of HL7 messages.  It will receive the message and write it to a log file.
 */
    $address="127.0.0.1";
    $port=14001;
    
    set_time_limit (2);
    if(false==($socket=  socket_create(AF_INET,SOCK_STREAM, SOL_TCP)))
    {
        echo "could not create socket";
    }
    socket_bind($socket, $address, $port) or die ("could not bind socket");
    socket_listen($socket);

    $resp = "MSH|^~\&|EPIC|EPIC|FLINTREG|FLINTREGISTRY|20190606111549||ACK|10000001|T|2.3\rMSA|AA|10000001\r";

    while(($client=socket_accept($socket))){
        echo "client is listening";
        while($input = socket_read($client, 2024)){
            
            $str = file_get_contents('adt_hl7.txt');
            file_put_contents('adt_hl7.txt', $str . $input);
            
            socket_write($client, chr(11).$resp.chr(28).chr(13));
        }
      }
