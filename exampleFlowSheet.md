# Example Flow Sheet message       

The following is an example of an flow sheet message that is sent by the interface code.  Custom values are highlighted to differentiate from the normal formatting code.  The template message can be seen at the bottom of [HL7Client.php](plugins/epic_integration/lib/HL7Client.php).

MSH|^\~\&|PLACEHOLDER|<code>433</code>|EPIC|PLACEHOLDER|20230717130116||ORU^R01|20230717AA5748FE-E547-4416|T|2.3\
PID|||<code>123456-0</code>^^^PLACEHOLDER^PLACEHOLDER~<code>Ga3xqYS97U</code>^^^PLACEHOLDER^FS||<code>DOE^JOHN</code>||20100101\
<code>OBR|||||||20230717130116</code>\
<code>OBX|1|ST|13768||Online Survey, by Participant (survey code)|||||||||20230717130116</code>\
<code>OBX|2|ST|13670||05/24/2023|||||||||20230717130116</code>\
<code>OBX|3|ST|14197||13|||||||||20230717130116</code>\
<code>OBX|4|ST|13727||Eligible (ELG)|||||||||20230717130116</code>\
<code>OBX|5|ST|14620||01/01/2021|||||||||20230717130116</code>\
<code>OBX|6|ST|13767||Online Survey, by Participant (survey code)|||||||||20230717130116</code>\
<code>OBX|7|ST|13728||Consented (CNT)|||||||||20230717130116</code>\
<code>OBX|8|ST|14619||Online Survey, by Participant (survey code)|||||||||20230717130116</code>\
<code>OBX|9|ST|13701||12/01/2022|||||||||20230717130116</code>\
<code>OBX|10|ST|10196||Online|||||||||20230717130116</code>\
<code>OBX|11|ST|19469||Complete|||||||||20230717130116</code>\
<code>OBX|12|ST|19956||Yes|||||||||20230717130116</code>\
<code>OBX|13|ST|19957||Yes|||||||||20230717130116</code>\
<code>OBX|14|ST|19958||Missing|||||||||20230717130116</code>\
<code>OBX|15|ST|24430||13|||||||||20230717130116</code>\
<code>OBX|16|ST|24433||Online-edition Survey, by Participant (using survey code)</code>|||||||||20230717130116

