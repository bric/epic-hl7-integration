# Example ADT message       

The following is an example of an ADT message that is sent by the interface code.  Custom values are highlighted to differentiate from the normal formatting code.  The template message can be seen at the bottom of [HL7Client.php](plugins/epic_integration/lib/HL7Client.php).

MSH|^\~\&|PLACEHOLDER|PLACEHOLDER|EPIC|PLACEHOLDER|20230524111306||ADT^A31|202305249A8BF5E3-E226-443A-BD8D-F76ED4465E95|T|2.3||||||\
PID|1||<code>123456-0</code>^^^FRE^FR~<code>Ga3xqYS97U</code>^^^PLACEHOLDER^FS||<code>DOE^JOHN</code>||20100101||||^^^^^USA^P||^P^PRN^^^^|^P^PRN^^^^||||||||||||||||N\
PV2|||||||\
NTE|||\
ZPD||||||||||||||
